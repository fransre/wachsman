window.addEventListener("load",
  function() {
    document.querySelectorAll("img").forEach(image => {
      image.addEventListener("click", function(event) {
        window.location.assign(event.target.src)
      })
    })
    document.querySelectorAll(".bronverwijzing").forEach(reference => {
      reference.addEventListener("click", function(event) {
        const was_chosen = event.target.classList.contains("gekozen")
        document.querySelectorAll(".gekozen").forEach(chosen => chosen.classList.remove("gekozen"))
        if (was_chosen)
          return
        const parts = event.target.innerText.split(",")
        const boundaries = parts.map(range => range.split("-").map(number => Number(number)))
        const ranges = boundaries.map(boundaries => Array.from(Array(boundaries.slice(-1)[0] + 1).keys()).slice(boundaries[0]))
        const ids = ranges.flat()
        const sources = ids.map(reference => document.querySelector(`#bron${reference}`))
        sources.concat([event.target]).forEach(element => element.classList.add("gekozen"))
        const first_source = sources[0]
        first_source.scrollIntoView()
      })
    })
  }
)
